import mongoose, { Schema } from 'mongoose';
import { logger } from '../../services/logger-service';
import { DialogElement } from './dialog';
import { Flow } from './flow';
import { IBot } from '../../types/bot';

const BotSchema = new Schema<IBot>(
  {
    id: {
      type: Number,
      unique: false,
    },
    description: {
      type: String,
      required: true,
    },
    name: {
      type: String,
      required: true,
    },
    token: {
      type: String,
      required: true,
    },
    dialogs: {
      dialog: DialogElement,
      flow: Flow,
    },
  },
  {
    timestamps: true,
    versionKey: false,
  }
);
BotSchema.post<IBot>('save', (doc) => {
  logger.info(`Bot ${doc.name} saved`);
});
const BotModel = mongoose.model<IBot>('Bot', BotSchema);
export { BotModel };
