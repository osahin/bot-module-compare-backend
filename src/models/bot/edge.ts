import { Schema } from 'mongoose';
import { IEdge } from '../../types/bot/edge';

export const Edge = new Schema<IEdge>({
  id: {
    type: String,
  },
  fromId: {
    type: String,
  },
  toId: {
    type: String,
  },
});
