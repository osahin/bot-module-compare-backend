import { Schema } from 'mongoose';
import { Node } from './node';
import { Edge } from './edge';
export const DialogElement = new Schema({
  edges: [Edge],
  nodes: [Node],
});
