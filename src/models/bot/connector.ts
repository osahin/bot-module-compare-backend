import { Schema } from 'mongoose';
import { IConnector } from '../../types/bot/connector';

export const Connector = new Schema<IConnector>({
  beginConnectionPointIndex: {
    type: Number,
  },
  beginItemKey: {
    type: String,
  },
  endConnectionPointIndex: {
    type: Number,
  },
  endItemKey: {
    type: String,
  },
  points: [
    {
      x: {
        type: Number,
      },
      y: {
        type: Number,
      },
    },
  ],
  locked: {
    type: Boolean,
  },
  key: {
    type: String,
  },
  dataKey: {
    type: String,
  },
  zIndex: {
    type: Number,
  },
});
