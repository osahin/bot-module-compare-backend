import { Schema } from 'mongoose';
import { IFlow } from '../../types/bot/flow';
import { Connector } from './connector';
import { Page } from './page';
import { Shape } from './shape';

export const Flow = new Schema<IFlow>({
  connectors: [Connector],
  page: Page,
  shapes: [Shape],
});
