import { Schema } from 'mongoose';
import { IShape } from '../../types/bot/shape';

export const Shape = new Schema<IShape>({
  dataKey: {
    type: String,
  },
  height: {
    type: Number,
  },
  key: {
    type: String,
  },
  locked: {
    type: Boolean,
  },
  text: {
    type: String,
  },
  type: {
    type: String,
  },
  width: {
    type: Number,
  },
  x: {
    type: Number,
  },
  y: {
    type: Number,
  },
  zIndex: {
    type: Number,
  },
});
