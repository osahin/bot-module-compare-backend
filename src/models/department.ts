import { model, Schema } from 'mongoose';
import { IDepartment } from '../types/department';

const departmentSchema = new Schema<IDepartment>(
  {
    name: {
      type: String,
      required: true,
    },
    company: {
      type: Schema.Types.ObjectId,
      ref: 'Company',
      required: true,
    },
  },
  { timestamps: true }
);

const Department = model<IDepartment>('Department', departmentSchema);

export { Department };
