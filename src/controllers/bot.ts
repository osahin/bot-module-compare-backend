import { Request, Response } from 'express';
import fetch from 'node-fetch';
import { logger } from '../services/logger-service';
import { BotModel } from '../models/bot';

const createBot = async (req: Request, res: Response) => {
  const bot = req.body;
  if (await BotModel.findOne({ token: bot.token })) {
    return res.status(400).json({ error: 'Bot is already exist' });
  }
  const botModel = new BotModel(bot);
  const savedBot = await botModel.save();
  // eslint-disable-next-line no-underscore-dangle
  fetch(`http://${process.env.TSGENERATOR_HOST}:${process.env.TSGENERATOR_PORT}/generate-ts/${savedBot._id}`).catch(
    (err: any) => {
      logger.error(err);
    }
  );
  res.status(201).json(botModel);
};
const getAllBots = async (_req: Request, res: Response) => {
  const bots = await BotModel.find({});
  res.status(200).json(bots);
};
const getBotByToken = async (req: Request, res: Response) => {
  const { token } = req.params;
  const bot = await BotModel.findOne({ token: token });
  res.status(200).json(bot);
};
const deleteBotByToken = async (req: Request, res: Response) => {
  const { token } = req.params;
  const botId = req.body._id;
  if (!botId && !token) {
    return res.status(400).json({ error: 'Invalid paramaters' });
  }

  const bot = await BotModel.findOneAndDelete({ token });
  if (!bot) {
    return res.status(400).json({ error: 'Bot could not update' });
  }
  res.status(200).json({
    deletedBot: bot.name,
  });
  fetch(`http://${process.env.PROCESSRUNNER_HOST}:${process.env.PROCESSRUNNER_PORT}/stop-bot/${bot._id}.ts`).catch(
    (err: any) => {
      logger.error(err);
    }
  );
};
const updateBot = async (req: Request, res: Response) => {
  const bot = req.body;

  const response = await fetch(
    `http://${process.env.PROCESSRUNNER_HOST}:${process.env.PROCESSRUNNER_PORT}/stop-bot/${bot._id}.ts`
  );

  if (!response.ok) {
    return res.status(500).json({ error: 'Bot could not stopped' });
  }

  // eslint-disable-next-line no-underscore-dangle
  delete bot._id;
  const botToken = bot.token;
  const targetBot = await BotModel.findOne({ botToken }).exec();

  if (!targetBot) {
    return res.status(400).json({ error: 'Target bot not found' });;
  }

  targetBot.set(bot);
  await targetBot.save();

  const generateResponse = fetch(
    `http://${process.env.TSGENERATOR_HOST}:${process.env.TSGENERATOR_PORT}/generate-ts/${targetBot._id}`
  );

  if (!generateResponse) {
    return res.status(400).json({ error: 'Bot could be not updated' });;
  }

  res.send(targetBot);
};
export { createBot, getAllBots, getBotByToken, deleteBotByToken, updateBot };
