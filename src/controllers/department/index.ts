import { Response, Request } from 'express';
import { Department } from '../../models/department';

const addDepartment = async (req: Request, res: Response): Promise<void> => {
  try {
    const { name, companyId } = req.body;

    const department = new Department({ name: name, company: companyId });
    await department.save();

    const departments = await Department.find().populate('company').exec();

    res.status(201).json({ message: 'Department added', department, departments });
  } catch (error) {
    throw error;
  }
};

export { addDepartment };
