import { Response, Request } from 'express';
import { Company } from '../../models/company';

const addCompany = async (req: Request, res: Response): Promise<void> => {
  try {
    const { name } = req.body;

    const company = new Company({ name });
    await company.save();

    const companies = await Company.find().exec();

    res.status(201).json({ message: 'Company added!', company: company, companies: companies });
  } catch (error) {
    throw error;
  }
};

export { addCompany };
