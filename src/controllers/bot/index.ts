import { Request, Response } from 'express';
import fetch from 'node-fetch';
import { logger } from '../../services/logger-service';
import { BotModel } from '../../models/bot';

const createBot = async (req: Request, res: Response) => {
  const botInstance = new BotModel(req.body);
  const createdBot = await BotModel.create(botInstance);
  const savedBot = await createdBot.save();
  if (!savedBot) {
    logger.warn('Bot could not be created.', savedBot);
    return res.status(500).json({ message: 'Bot could not be created!' });
  }
  fetch(`http://${process.env.TSGENERATOR_HOST}:${process.env.TSGENERATOR_PORT}/generate-ts/${savedBot._id}`).catch(
    (err: any) => {
      logger.error(err);
      return res.status(500).json({ error: 'Bot could not updated' });
    }
  );
  res.status(200).json(savedBot);
};
const getAllBots = async (_req: Request, res: Response) => {
  const bots = await BotModel.find().exec();
  res.status(200).json(bots);
};
const getBotByToken = async (req: Request, res: Response) => {
  const { token } = req.params;
  const bot = await BotModel.findOne({ token }).exec();
  res.status(200).json(bot);
};
const updateBot = async (req: Request, res: Response) => {
  const bot = req.body;
  const response = await fetch(
    `http://${process.env.PROCESSRUNNER_HOST}:${process.env.PROCESSRUNNER_PORT}/stop-bot/${bot._id}.ts`
  );
  if (!response.ok) {
    return res.status(500).json({ error: 'Bot could not stopped' });
  }
  const updatedBot = await BotModel.findByIdAndUpdate(bot._id, bot, {
    new: true,
    upsert: false,
    remove: false,
    projection: {},
  }).exec();
  if (!updatedBot) {
    return res.status(400).json({ error: 'Bot not updated' });
  }
  // eslint-disable-next-line no-underscore-dangle
  fetch(`http://${process.env.TSGENERATOR_HOST}:${process.env.TSGENERATOR_PORT}/generate-ts/${updatedBot._id}`).catch(
    (err: any) => {
      logger.error(err);
      return res.status(500).json({ error: 'Bot could not updated' });
    }
  );
  res.status(200).json(updatedBot);
};
const deleteBotByToken = async (req: Request, res: Response) => {
  const { token } = req.params;
  const bot = await BotModel.findOne({ token }).exec();
  if (!bot) {
    return res.status(400).json({ message: 'The bot that was supposed to be deleted was not found!' });
  }
  const response = await fetch(
    `http://${process.env.PROCESSRUNNER_HOST}:${process.env.PROCESSRUNNER_PORT}/stop-bot/${bot._id}.ts`
  );
  if (!response.ok) {
    return res.status(500).json({ error: 'The bot could not be stopped!' });
  }
  const deletedBot = await BotModel.findByIdAndDelete(bot._id).exec();
  res.status(202).json({ message: `The bot was deleted successfully.`, deletedBot: deletedBot });
};
export { createBot, getAllBots, getBotByToken, updateBot, deleteBotByToken };
