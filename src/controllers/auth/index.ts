import { Request, Response } from 'express';
import { JwtService } from '../../services/jwt-service';
import { PasswordService } from '../../services/password-service';
import { BadRequestError } from '../../errors/bad-request-error';
import { User } from '../../models/user';

const signup = async (req: Request, res: Response) => {
  const { firstName, lastName, email, username, password, avatar, department } = req.body;

  let existingUser = await User.findOne({ username }).exec();

  if (existingUser) {
    throw new BadRequestError('Username in use');
  }

  existingUser = await User.findOne({ email }).exec();

  if (existingUser) {
    throw new BadRequestError('Email in use');
  }

  const user = await User.create({ firstName, lastName, email, username, password, avatar, department });
  await user.save();

  const userJwt = JwtService.sign({ id: user._id, email: user.email, username: user.username }, 'Hello');

  req.session = {
    jwt: userJwt,
  };

  res.status(201).send(user);
};

const login = async (req: Request, res: Response) => {
  const { username, password } = req.body;

  const invalidCredentialsMessage = 'Invalid credentials';

  const existingUser = await User.findOne({ username }).exec();
  if (!existingUser) {
    throw new BadRequestError(invalidCredentialsMessage);
  }

  const passwordsMatch = await PasswordService.compare(existingUser.password, password);
  if (!passwordsMatch) {
    throw new BadRequestError(invalidCredentialsMessage);
  }

  const userJwt = JwtService.sign(
    { id: existingUser._id, email: existingUser.email, username: existingUser.username },
    'Hello'
  );

  req.session = {
    jwt: userJwt,
  };

  res.status(200).send({ severity: 'success', summary: 'Works', detail: 'Logged in successfully.', isSuccess: true });
};

const logout = (req: Request, res: Response) => {
  req.session = null;
  res.send({ result: 'Logged out successfully.' });
};

const currentUser = (req: Request, res: Response) => {
  res.send({ currentUser: req.currentUser || null });
};

export { signup, login, logout, currentUser };
