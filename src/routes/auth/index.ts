import { Router } from 'express';
import { currentUser as currentUserMiddleware } from '../../middlewares/current-user';
import { login, logout, signup, currentUser as currentUserController } from '../../controllers/auth';

const router: Router = Router();

router.post('/signup', signup);

router.post('/login', login);

router.post('/logout', logout);

router.get('/current-user', currentUserMiddleware, currentUserController);

export { router as authRouter };
