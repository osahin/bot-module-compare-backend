import { Router } from 'express';
import { addCompany } from '../../controllers/company';

const router: Router = Router();

router.post('/add', addCompany);

export { router as companyRouter };
