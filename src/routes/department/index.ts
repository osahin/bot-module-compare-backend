import { Router } from 'express';
import { addDepartment } from '../../controllers/department';

const router: Router = Router();

router.post('/add', addDepartment);

export { router as departmentRouter };
