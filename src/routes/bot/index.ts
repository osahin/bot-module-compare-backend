import express from 'express';
import { body } from 'express-validator';
import { getBotByToken, getAllBots, updateBot, createBot, deleteBotByToken } from '../../controllers/bot';
import { validateRequest } from '../../middlewares/validate-request';

const router = express.Router();
router.post('/create', createBot);
router.get('/get-all-bots', getAllBots);
router.get('/get-bot-by-token/:token', getBotByToken);
router.put(
  '/update',
  [
    body('token').not().isEmpty().withMessage('Bot token is required'),
    body('name').not().isEmpty().withMessage('Bot name is required'),
    body('dialogs').not().isEmpty().withMessage('Bot dialogs is required'),
    body('description').not().isEmpty().withMessage('Bot description is required'),
  ],
  validateRequest,
  updateBot
);
router.delete('/:token', deleteBotByToken);
export { router as botRouter };
