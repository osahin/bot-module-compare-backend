import { Request, Response, NextFunction } from 'express';
import { logger } from '../services/logger-service';
import { IUserPayload } from '../types/user-payload';
import { JwtService } from '../services/jwt-service';

export const currentUser = (req: Request, res: Response, next: NextFunction) => {
  if (!req.session?.jwt) {
    return next();
  }

  try {
    const payload = JwtService.verify(req.session.jwt) as IUserPayload;
    req.currentUser = payload;
  } catch (err) {
    logger.error((err as Error).message, { req, res });
  }

  return next();
};
