import express, { Request, Response } from 'express';
import 'express-async-errors';
import { json } from 'body-parser';
import cors from 'cors';
import { errorLogger } from 'express-winston';
import helmet from 'helmet';
import router from './routes/index';
import { errorHandler } from './middlewares/error-handler';
import { logger, requestLogger } from './services/logger-service';
import { NotFoundError } from './errors/not-found-error';
import cookieSession from 'cookie-session';

const app = express();

app.disable('x-powered-by');
app.set('trust proxy', true);
app.use(json());
app.use(
  cors({
    origin: true,
    credentials: true,
  })
);
app.use(helmet());
app.use(
  cookieSession({
    signed: false,
    secure: process.env.NODE_ENV === 'production',
  })
);
/** logging the req */
app.use(requestLogger);

/** Parsing for json object */

/** Routing the main in '/source/routes/main' */
app.use('/api', router);

/** Error Handling */
app.all('*', async (req: Request, res: Response) => {
  logger.error(`The requested path is not found.`, { req, res });
  throw new NotFoundError();
});

app.use(errorLogger);
app.use(errorHandler);

/** Listen server */
export { app };
