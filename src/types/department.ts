import { Document } from 'mongoose';
import { ICompany } from './company';

export interface IDepartment extends Document {
  name: string;
  company: ICompany;
}
