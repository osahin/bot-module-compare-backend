interface IUserPayload {
  id: string;
  email: string;
  username: string;
}

export { IUserPayload };
