import { Document } from 'mongoose';
import { IDepartment } from './department';

export interface IUser extends Document {
  firstName: string;
  lastName: string;
  email: string;
  username: string;
  password: string;
  avatar: string;
  department: IDepartment;
}
