export interface IUser {
  last_name: string;
  is_bot: boolean;
  first_name: string;
  id: number;
  username: string;
  language_code: string;
  photos: string[];
}
