import { Contact } from './contact';
import { Photo } from './photo';
import { Voice } from './voice';

export interface Message {
  question: { text: string; date_time: Date };

  answer: {
    text?: string;
    location?: Location;
    voice?: Voice;
    photo?: Photo[];
    data?: string;
    contact?: Contact;
    date_time: Date;
  };
}
