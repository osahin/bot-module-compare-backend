export interface Location {
  /** Longitude as defined by sender */
  longitude: number;
  /** Latitude as defined by sender */
  latitude: number;
  /** The radius of uncertainty for the location, measured in meters; 0-1500 */
  horizontal_accuracy?: number;
}
