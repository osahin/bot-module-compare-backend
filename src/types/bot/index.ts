import { IDialog } from './dialog';
import { IFlow } from './flow';

export interface IBot {
  id: number;
  name: string;
  token: string;
  description: string;
  dialogs: { dialog: IDialog[]; flow: IFlow };
}
