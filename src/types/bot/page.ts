export interface IPage extends Document {
  height: number;
  width: number;
  pageColor: number;
  pageHeight: number;
  pageLandspace: boolean;
  pageWidth: number;
}
