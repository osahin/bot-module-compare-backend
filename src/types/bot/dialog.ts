import { IEdge } from './edge';
import { INode } from './node';

export interface IDialog {
  edges: Array<IEdge>;
  nodes: Array<INode>;
}
