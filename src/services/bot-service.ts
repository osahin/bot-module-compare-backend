import { request } from 'http';

const options = {
  host: 'localhost',
  port: 5000,
  path: '/api/start',
  method: 'POST',
  headers: {
    'Content-Type': 'application/json',
    'Content-Length': 0,
  },
};
const startBotScript = (rawToken: string) => {
  const token = JSON.stringify({ token: rawToken });
  options.headers['Content-Length'] = rawToken.length;
  options.path = '/api/start';
  const botRequest = request(options, (response) => {
    if (response.statusCode === 201) {
      return true;
    }
    return false;
  });
  botRequest.write(token);
  botRequest.end();
};

const updateBotScript = (rawToken: string) => {
  const token = JSON.stringify({ token: rawToken });
  options.headers['Content-Length'] = token.length;
  options.path = '/api/update';
  const botRequest = request(options, (response) => {
    if (response.statusCode === 204) {
      return true;
    }
    return false;
  });
  botRequest.write(token);
  botRequest.end();
};

export { startBotScript, updateBotScript };
