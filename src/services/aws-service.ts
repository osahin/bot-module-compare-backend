import { Request } from 'express';
import multer, { FileFilterCallback } from 'multer';
import multerS3 from 'multer-s3';
import { S3Client, PutObjectCommand } from '@aws-sdk/client-s3';
import path from 'path';
import shortid from 'shortid';
import fetch from 'node-fetch';

if (!process.env.ENDPOINT || !process.env.BUCKET || !process.env.ACCESS_KEY_ID || !process.env.SECRET_ACCESS_KEY) {
  throw new Error(
    'Make sure ENDPOINT, BUCKET, ACCESS_KEY_ID, and SECRET_ACCESS_KEY environment variables are defined.'
  );
}

export const s3 = new S3Client({
  endpoint: process.env.ENDPOINT,
  region: 'us-east-1',
  credentials: {
    accessKeyId: process.env.ACCESS_KEY_ID,
    secretAccessKey: process.env.SECRET_ACCESS_KEY,
  },
});
multer.diskStorage({
  filename: () => {},
});
const storage = multerS3({
  s3,
  bucket: process.env.BUCKET,
  //   acl: "public-read",
  contentType: multerS3.AUTO_CONTENT_TYPE,
  metadata: (_req, file, cb) => {
    cb(null, { fieldName: file.fieldname });
  },
  key: (_req, file, cb) => {
    cb(null, shortid.generate() + '-' + file.originalname);
  },
});

const fileFilter = (req: Request, file: Express.MulterS3.File, cb: FileFilterCallback) => {
  const filetypes = /jpeg|jpg|png|oga/;
  const extname = filetypes.test(path.extname(file.originalname).toLowerCase());
  const mimetype = filetypes.test(file.mimetype);
  if (mimetype && extname) {
    return cb(null, true);
  } else {
    cb(new Error('Error: Allow images only of extensions jpeg|jpg|png !'));
  }
};

const limits = { fileSize: 1024 * 1024 * 50 };

const upload = multer({
  storage,
  limits,
  fileFilter,
});
const conversationFileUpload = async (url: string, fileName: string) => {
  try {
    // URL'den veri çekin
    let response;
    try {
      response = await fetch(url);
    } catch (err) {
      console.error(`Error fetching the file: ${err}`);
      return;
    }
    const fileData = await response.arrayBuffer();

    // Dosyayı S3'e yükle
    try {
      await s3.send(
        new PutObjectCommand({
          Bucket: process.env.CONVERSATION_BUCKET,
          Key: fileName,
          Body: Buffer.from(fileData),
          ContentType: response.headers.get('Content-Type') || 'application/octet-stream',
          ContentLength: parseInt(response.headers.get('Content-Length') as string),
          ContentEncoding: response.headers.get('Content-Encoding') || undefined,
        })
      );
      console.log(`File uploaded successfully: ${fileName}`);
    } catch (err) {
      console.error(`Error uploading the file: ${err}`);
    }
  } catch (err) {
    console.error(err);
  }
};

export { upload, conversationFileUpload };
