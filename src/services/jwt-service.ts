import fs from 'fs';
import jwt from 'jsonwebtoken';
import { IUserPayload } from '../types/user-payload';

const privateKEY = fs.readFileSync('./secrets/private.key', 'utf8');
const publicKEY = fs.readFileSync('./secrets/public.key', 'utf8');

export class JwtService {
  static issuer = 'A Grup';

  static audience = 'https://akimmetal.com.tr';

  static expiresIn = '5d';

  static algorithm: jwt.Algorithm = 'RS256';

  static sign(payload: IUserPayload, subject: string) {
    const signOptions: jwt.SignOptions = {
      issuer: this.issuer,
      subject,
      audience: this.audience,
      expiresIn: this.expiresIn,
      algorithm: this.algorithm,
    };
    return jwt.sign(payload, privateKEY, signOptions);
  }

  /**
   * Verifies the given token.
   * On invalid token, throws an error.
   * @param token the jwt token to be verified.
   * @returns - the payload decoded if the signature is valid and optional expiration, audience, or issuer are valid. If not, it will throw the error.
   */
  static verify(token: string) {
    const verifyOptions: jwt.VerifyOptions = {
      issuer: this.issuer,
      audience: this.audience,
      algorithms: [this.algorithm],
    };

    try {
      return jwt.verify(token, publicKEY, verifyOptions);
    } catch (err) {
      return false;
    }
  }

  static decode(token: string) {
    return jwt.decode(token, { complete: true });
  }
}
