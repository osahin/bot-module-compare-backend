import 'dotenv/config';
import mongoose from 'mongoose';
import { app } from './app';
import { logger } from './services/logger-service';

const PORT = process.env.PORT || 4000;

const databaseConnection = async () => {
  if (!process.env.MONGO_CONNECTION_URL) {
    throw Error('"MONGO_CONNECTION_URL" environment variable must be defined!');
  }

  try {
    await mongoose.connect(process.env.MONGO_CONNECTION_URL, {
      socketTimeoutMS: 30000,
      keepAlive: true,
      autoIndex: false,
      retryWrites: false,
      dbName: 'BotModule',
    });
    logger.info('Database connection successful.');
  } catch (error) {
    logger.error(error);
    setTimeout(databaseConnection, 3000);
  }
};

/**
 * The application needs to be started in this way for enabling Jest tests.
 */
const start = async () => {
  await databaseConnection();

  app.listen(PORT, () => {
    logger.info(`Initialization successful -> Listening on port ${PORT}!`);
  });
};

start();
