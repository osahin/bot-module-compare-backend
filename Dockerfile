FROM node:19-alpine@sha256:d0b02b1ec5534efb43a926069915c982aec745a8eb0611ebcffc4cafaa4e4a74 as build
WORKDIR /app
# copy dependency file from host machine
COPY package.json .
# install dependencies

ENV NODE_TLS_REJECT_UNAUTHORIZED=0
RUN npm config set strict-ssl false
RUN npm install
# copy source code
COPY . .
# run build
RUN npm run build

FROM node:19-alpine@sha256:d0b02b1ec5534efb43a926069915c982aec745a8eb0611ebcffc4cafaa4e4a74 as deploy
# set environment variable
ENV MONGO_CONNECTION_URL='localhost'
ENV ENDPOINT='localhost'
ENV BUCKET='isg'
ENV ACCESS_KEY_ID='accessKey'
ENV SECRET_ACCESS_KEY='secretKey'
ENV CONVERSATION_BUCKET='conversation'
ENV TSGENERATOR_HOST='localhost'
ENV TSGENERATOR_PORT=80
ENV PROCESSRUNNER_HOST='localhost'
ENV PROCESSRUNNER_PORT=80
ENV PORT=80
ENV NODE_ENV='production'
# install dumb-init package for gracefull stop with child process
RUN apk add dumb-init

WORKDIR /app

# get dependency file from build stage
COPY --from=build /app/package.json .
COPY --from=build /app/package-lock.json .
# install only PRODUCTION dependencies
RUN npm ci --only=production

# copy source code
COPY --from=build /app/dist/ .

COPY --from=build /app/secrets/ ./secrets/

# change user 
USER node
CMD ["dumb-init","node", "./index.js"]
EXPOSE 80